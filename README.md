# A web application for delivery of hygienic foods from home to clients
* Live: (Still on development server , Soon it will go live to production server)

## Install and active virtual-environment by command:
* pip install virtualenv
* virtualenv your_environment_name
* active

## Install all the requirements by command:
* pip install -r requirements.txt

